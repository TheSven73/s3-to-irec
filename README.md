Firmware SREC to IHEX Converter
===============================

Simple tool to convert firmware from
[SREC format](https://en.wikipedia.org/wiki/SREC_(file_format)) to
[Intel HEX format](https://en.wikipedia.org/wiki/Intel_HEX).

Usage:
```
cat myfirmware.s3 | python3 srec_to_ihex.py > myfirmware.ihex
```

#!/usr/bin/python3
# SPDX-License-Identifier: GPL-2.0-only

# Tool to convert .s3 firmware files into their .ihex equivalent.
# Usage: cat myfirmware.s3 | python3 srec_to_ihex.py > myfirmware.ihex

import sys

def wrap_line(line):
	"""Wraps a line with a colon (:) and a checksum"""
	checksum = 0
	for idx in range(0, len(line), 2):
		checksum += int(line[idx:idx+2], 16)
	# twos complement
	checksum = (~checksum + 0x101) & 0xFF
	return ':{}{:02X}'.format(line, checksum)

def handle_s3(line):
	addr = line[4:12]
	payload = line[12:][:-2]
	# we have 32-bit addresses, so we must use extended segment address
	result = '02000004' + addr[:4]
	print(wrap_line(result))
	# followed by the data itself
	result = '{:02X}{}00{}'.format(len(payload)//2, addr[4:], payload)
	print(wrap_line(result))

def handle_s7(line):
	addr = line[4:12]
	result = '04000005' + addr
	print(wrap_line(result))

if __name__ == "__main__":
	for line in sys.stdin:
		line = line.strip()
		stype = line[:2]
		if stype == 'S3':
			handle_s3(line)
		elif stype == 'S7':
			handle_s7(line)
		else:
			print('ignored line:', line, file=sys.stderr)
	print(':00000001FF')
